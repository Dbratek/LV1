#Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj petlji 
#sve dok korisnik ne upiše „Done“ (bez navodnika). Nakon toga potrebno je 
#ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i 
#maksimalnu vrijednost. Osigurajte program od krivog unosa (npr. slovo umjesto
#brojke) na način da program zanemari taj unos i ispiše odgovarajuću poruku.

zbroj = 0
brx = 0
a=[]
while ( 1):
    x = raw_input('Unesi broj: ')
    if x == 'done' : break
    bx = float(x)
    a.insert(brx,bx)    #zapis u polju
    zbroj = zbroj + bx  #zbroj svih unesenih vrijednosti
    brx = brx + 1       #brojač unosa
srvr = zbroj / brx
print 'Unesene vrijednosti:', a
print 'Srednja vrijednost:', srvr
print 'Max vrijednost:',max(a)
print 'Mini vrijednost:',min(a)
print 'Broj unosa:',brx